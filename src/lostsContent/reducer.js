import {FETCH_STARTED, FETCH_SUCCESS, FETCH_FAILURE, CLEAR_LOSTSCONTENT_DATA} from './actionTypes.js';
import * as Status from './status.js';

export default (state = {status: Status.LOADING, lostsContent: []}, action) => {
	switch(action.type) {
	case FETCH_STARTED: {
		return {...state, status: Status.LOADING};
    }
    case FETCH_SUCCESS: {
		return {...state, status: Status.SUCCESS, lostsContent: [...state.lostsContent, ...action.result.hits], page: action.result.page, nbPages: action.result.nbPages};
    }
    case FETCH_FAILURE: {
		return {...state, status: Status.FAILURE};
    }
	case CLEAR_LOSTSCONTENT_DATA: {
		return {...state, lostsContent: []}
	}
    default: {
      return state;
    }
  }
}
