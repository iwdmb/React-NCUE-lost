import * as actions from './actions.js'
import reducer from './reducer.js'
import view from './views/analysis.js'

export {actions, reducer, view};
