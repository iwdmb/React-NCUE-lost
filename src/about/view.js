import React from 'react'
import { Layout } from 'antd';
//import axios from 'axios';
import './style.css';

class about extends React.Component {
	/*
	constructor(props) {
		super(props)
	}
	*/

	render() {
		return (
			<Layout className="content">
				<h1>關於</h1>
				<p>NCUE 失物招領系統由國立彰化師範大學學生會開發及維護，目標希望藉由此系統提升校內學生遺失物找回的機會，以及方便各單位更有系統的管理校內遺失物。</p>
				<p>使用上若有遇到任何問題、建議，歡迎與我們聯絡 contact@su.ncue.org</p>
				<p>: )</p>
			</Layout>
		)
	}
}

export default about
