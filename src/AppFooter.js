import React from 'react';
import {Breadcrumb, Layout} from 'antd';

const {Footer} = Layout;

class AntdFooter extends React.Component {
	render() {
		return (
			<Footer style={{ textAlign: 'center' }}>
			<Breadcrumb>
				<Breadcrumb.Item>© 2018 <a href="https://su.ncue.org/">NCUE Student Union</a></Breadcrumb.Item>
				<Breadcrumb.Item><a href="mailto:contact@su.ncue.org">contact@su.ncue.org</a></Breadcrumb.Item>
				<Breadcrumb.Item>Maintained by <a href="https://fb.com/bmiwdmb">@Ming</a></Breadcrumb.Item>
			</Breadcrumb>
			</Footer>
		);
	}
}

export default AntdFooter
