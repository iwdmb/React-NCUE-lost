import {FETCH_STARTED, FETCH_SUCCESS, FETCH_FAILURE, CLEAR_LOSTSANALYSIS_DATA} from './actionTypes.js';
import * as Status from './status.js';

export default (state = {status: Status.LOADING, analysisContent: {}}, action) => {
	//console.log(action.type)
	switch(action.type) {
	case FETCH_STARTED: {
		return {...state, status: Status.LOADING};
    }
    case FETCH_SUCCESS: {
		return {...state, status: Status.SUCCESS, analysisContent: {...state.analysisContent, ...action.obj}}
    }
    case FETCH_FAILURE: {
		return {...state, status: Status.FAILURE};
    }
	case CLEAR_LOSTSANALYSIS_DATA: {
		return {...state, analysisContent: {}}
	}
    default: {
      return state;
    }
  }
}
