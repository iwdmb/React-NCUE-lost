import React from 'react';
import { Menu, Layout } from 'antd';
import { Link } from "react-router-dom";
import path from 'path'
import axios from 'axios';
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import {actions as lostsContentActions} from '../../lostsContent';
//import { enquireScreen } from '../../utils';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const { Sider } = Layout;

class CustomSider extends React.Component {
	static propTypes = {
		match: PropTypes.object.isRequired,
		location: PropTypes.object.isRequired,
		history: PropTypes.object.isRequired
	}

	constructor(props) {
		super(...arguments);

		//const { match, location, history } = props;
		const { match } = props;

		this.state = {
			childArr: [],
			parentArr: [],
			locationPathDirName: path.dirname("/losts/*"),
			routerId: match.params.id
		};

		this.componentDidMount = this.componentDidMount.bind(this);
	}

	componentDidMount() {
		//sider
		axios({
			method: 'get',
			url: `${process.env.REACT_APP_API_URI}/v1/categorys`,
			responseType: 'json'
		}).then(function(response) {
			//console.log(response)

			let childArr = [];
			let parentArr = [];

			response.data.hits.forEach(function(element) {
				if (element.parent_id === 0) {
					childArr[element.id] = [];
					parentArr[element.id] = element;
				} else {
					childArr[element.parent_id].push(element);
				}
			});

			childArr.forEach(function(element) {
				element.sort(function(lhs, rhs) {
					return lhs.id > rhs.id;
				});
			});

			this.setState({childArr: childArr, parentArr: parentArr});
		}.bind(this));

		// content
		this.props.clearLostsContentData()
		const defaultCity = this.state.routerId || 0
		this.props.onSelectSubCategory(defaultCity, 1, 10)
	}

	handleClick = (e) => {
		this.props.clearLostsContentData()
		const defaultCity = e.key || 0
		this.props.onSelectSubCategory(defaultCity, 1, 10)
	}

	render() {
		//const {location, match} = this.props;
		const {match} = this.props;
		const {childArr, parentArr, locationPathDirName} = this.state;

		return (
			<Sider breakpoint="lg" style={{background: '#ffffff'}} collapsedWidth="0" onCollapse={(collapsed, type) => { console.log(collapsed, type); }}>
				<Menu
					onClick={this.handleClick}
					style={{witdh: 256}}
					selectedKeys={[match.params.id ? match.params.id : '0']}
					defaultOpenKeys={['sub1']}
					mode="inline"
					theme="light"
				>
					<SubMenu key="sub1" title="遺失物分類">
						<Menu.Item key="0">
							<Link to={locationPathDirName}>
							所有分類
							</Link>
						</Menu.Item>
						{parentArr.map(function(element, index) {
							return(<MenuItemGroup key={"g" + index} title={element.name}>
								{childArr[element.id].map(function(element, index) {
									return (
										<Menu.Item key={element.id}>
											<Link to={path.join(locationPathDirName, element.id.toString())}>{element.name}</Link>
										</Menu.Item>
									)
								})}
							</MenuItemGroup>)
						})}
					</SubMenu>
				</Menu>
			</Sider>
		)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onSelectSubCategory: (subCategoryId, requestPage, hitsPerPage) => {
			dispatch(lostsContentActions.fetchLostsContent(subCategoryId, requestPage, hitsPerPage))
		},
		clearLostsContentData: () => {
			dispatch(lostsContentActions.clearLostsContentData())
		}
	}
};

export default withRouter(connect(null, mapDispatchToProps)(CustomSider))
