import { withRouter } from 'react-router'
import React from 'react';
import { Menu, Popover, Row, Col } from 'antd';
import { Link } from "react-router-dom";
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faBars from '@fortawesome/fontawesome-free-solid/faBars'
import { enquireScreen } from './utils';

class AntdHeader extends React.Component {
	state = {
		inputValue: '',
		menuVisible: false,
		menuMode: 'horizontal',
		searchOption: [],
		searching: false,
	};


	handleHideMenu = () => {
		this.setState({
			menuVisible: false,
		});
	}

	handleShowMenu = () => {
		this.setState({
			menuVisible: true,
		});
	}

	onMenuVisibleChange = (visible) => {
		this.setState({
			menuVisible: visible,
		});
	}

	handleSelect = (value) => {
		//location.href = value;
	}

	componentDidMount() {
		enquireScreen((b) => {
			this.setState({ menuMode: b ? 'inline' : 'horizontal' });
		});
	}

	render() {
		//const {inputValue, menuMode, menuVisible, searchOption, searching} = this.state
		const {menuMode, menuVisible } = this.state
		//console.log(menuMode);
		//const { location } = this.props;
		//

		//const { match, location, history } = this.props;
		const { location } = this.props;

		const path = location.pathname;
		const module = location.pathname.replace(/(^\/|\/$)/g, '').split('/').slice(0, -1).join('/');

		let activeMenuItem = module || 'losts';
		if (/^\/department/.test(path)) {
			activeMenuItem = 'department';
		} else if (/^\/analysis/.test(path)) {
			activeMenuItem = 'analysis';
		} else if (/^\/about/.test(path)) {
			activeMenuItem = 'about';
		} else if (path === '/') {
			activeMenuItem = 'losts';
		}

		//let activeMenuItem = 'home';

		const menu = (
			<Menu mode={menuMode} selectedKeys={[activeMenuItem]} id="nav" key="nav">
				<Menu.Item key="losts">
					<Link to="/losts">失物招領</Link>
				</Menu.Item>
				<Menu.Item key="department">
					<Link to="/department">保管單位</Link>
				</Menu.Item>
				<Menu.Item key="analysis">
					<Link to="/analysis">相關分析</Link>
				</Menu.Item>
				<Menu.Item key="about">
					<Link to="/about">關於</Link>
				</Menu.Item>
			</Menu>
		);

		return (
		  <div id="header" className="header">
			{menuMode === 'inline' ? (
			  <Popover
				overlayClassName="popover-menu"
				placement="bottomRight"
				content={menu}
				trigger="click"
				visible={menuVisible}
				arrowPointAtCenter
				onVisibleChange={this.onMenuVisibleChange}
			  >
				<FontAwesomeIcon className="nav-phone-icon" icon={faBars} onClick={this.handleShowMenu} size="lg" />
			  </Popover>
			) : null}

        <Row>
          <Col xxl={4} xl={5} lg={8} md={8} sm={24} xs={24}>
			  <div id="logo">
			  	NCUE 失物招領
			  </div>
          </Col>
          <Col xxl={20} xl={19} lg={16} md={16} sm={0} xs={0}>
			<div className="header-meta">
			  {menuMode === 'horizontal' ? <div id="menu">{menu}</div> : null}
            </div>
          </Col>
        </Row>
      </div>
		);
	}
}

export default withRouter(AntdHeader)
