import React from 'react';
import { withRouter } from 'react-router'
import { connect } from 'react-redux';
import { message, Button, Spin, List, Icon, Layout } from 'antd';
import * as moment from 'moment';
import 'moment/locale/zh-tw'
import path from 'path'
import * as Status from '../status.js'
import {actions as lostsContentActions} from '../../lostsContent';
import ImageZoom from 'react-medium-image-zoom'

const { Content } = Layout;

class CustomContent extends React.Component {
	constructor(props) {
		super(...arguments);

		let loadingMore = false

		switch(this.props.lostsContentData.status) {
			case Status.LOADING: {
				loadingMore = true
				break
			}
			case Status.SUCCESS: {
				loadingMore = false
				break
			}
			default: {

			}
		}

		this.state = {
			loading: true,
			loadingMore: loadingMore,
			showLoadingMore: true,
			data: [],
			lastId: this.props.match.params.id ? this.props.match.params.id : 0,
		}
	}

  onLoadMore = () => {
    this.setState({
      loadingMore: true,
    });

	const defaultCity = this.props.match.params.id ? this.props.match.params.id : 0

	this.props.onLoadMore(defaultCity, this.props.lostsContentData.page + 1, 10)

	if (this.props.lostsContentData.page === this.props.lostsContentData.nbPages || this.props.lostsContentData.nbPages === 0) {
      message.warning('已經沒有更多遺失物囉！');
      this.setState({
        showLoadingMore: false,
        loadingMore: false,
      });
      return;
    }

    this.setState({
      loadingMore: false,
    });
  }

	render() {

	const IconText = ({ type, text }) => (
		<span>
			<Icon type={type} style={{ marginRight: 8 }} />
			{text}
		</span>
	);

	//const { loading, loadingMore, showLoadingMore, data, lastId } = this.state;
	const { showLoadingMore, lastId } = this.state;
	let { loadingMore } = this.state;

	// hard code
	if (this.props.lostsContentData.status === Status.SUCCESS) {
		loadingMore = false
	}

	const matchpid = this.props.match.params.id ? this.props.match.params.id : 0
	if (lastId !== matchpid) {
		this.setState({
			lastId: matchpid,
			showLoadingMore: true,
			loadingMore: false,
		})
	}

    const loadMore = showLoadingMore ? (
      <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
        {loadingMore && <Spin />}
        {!loadingMore && <Button onClick={this.onLoadMore}>loading more</Button>}
      </div>
    ) : null;

	return (
<div>
	<Content style={{ margin: '0 0px 0px 0px' }}>

		<div style={{ padding: '0 0 0 0', background: '#fff', minHeight: 360 }}>

			<List
				itemLayout="vertical"
				size="large"
				dataSource={this.props.lostsContentData.lostsContent}
				loadMore={loadMore}
				renderItem={item => (
					<List.Item
						key={item.id}
						actions={[<IconText type="user" text={item.last_name + item.first_name} />, <IconText type="environment" text={item.department_name} />, <IconText type="phone" text={item.tel} />]}
						extra={<ImageZoom
image={{
	src: path.dirname(item.image_link) + "/thumbnail-" + path.basename(item.image_link),
	alt: 'lost-item-image-thumbnail',
	className: 'img',
	style: { width: '272px' }
}}
zoomImage={{
	src: item.image_link,
	alt: 'lost-item-image'
}}
      />}
					>
						<List.Item.Meta
							title={item.name}
							description={moment(item.created_at).locale('zh-tw').fromNow()}
						/>
						物品編號：{item.id}<br />
						物品狀態：{(item.go_home ? "已認領" : "招領中")}<br />
						拾獲地點：{item.location_name}
					</List.Item>
				)}
			/>
		</div>
	</Content>

</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		lostsContentData: state.lostsContent
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onLoadMore: (subCategoryId, page, hitsPerPage) => {
			dispatch(lostsContentActions.fetchLostsContent(subCategoryId, page, hitsPerPage))
		}
	}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CustomContent))
