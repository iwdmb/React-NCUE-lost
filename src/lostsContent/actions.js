import {FETCH_STARTED, FETCH_SUCCESS, FETCH_FAILURE, CLEAR_LOSTSCONTENT_DATA} from './actionTypes.js'
import axios from 'axios';

export const fetchLostsContentStarted = () => ({
  type: FETCH_STARTED
});

export const fetchLostsContentSuccess = (subCategoryId, result) => ({
  type: FETCH_SUCCESS,
  result
})

export const fetchLostsContentFailure = (error) => ({
  type: FETCH_FAILURE,
  error
})

export const clearLostsContentData = () => ({
	type: CLEAR_LOSTSCONTENT_DATA
})

export const fetchLostsContent = (subCategoryId, page, hitsPerPage) => {
	return (dispatch) => {
		const apiUrl = `${process.env.REACT_APP_API_URI}/v1/lostpropertys?&subCategoryId=${subCategoryId}&page=${page}&hitsPerPage=${hitsPerPage}`

		dispatch(fetchLostsContentStarted())

		axios({
			method: 'get',
			url: apiUrl,
			responseType: 'json'
		}).then((response) => {
			if (response.status !== 200) {
				throw new Error('Fail to get response with status ' + response.status);
			}

			dispatch(fetchLostsContentSuccess(subCategoryId, response.data))
		})
		/*
		.catch((error) => {
			dispatch(fetchLostsContentFailure(error))
		})
		*/
	}
}
