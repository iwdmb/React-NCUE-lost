//import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import {createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk'
import {reducer as lostsContentReducer} from './lostsContent';
import {reducer as analysisReducer} from './analysis';

//import { routerReducer, routerMiddleware, push } from 'react-router-redux'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

const history = createHistory()
const routerWithHistoryMiddleware = routerMiddleware(history)

const reducer = combineReducers ({
	lostsContent: lostsContentReducer,
	analysis: analysisReducer,
	router: routerReducer
});

//const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), applyMiddleware(thunkMiddleware, routerWithHistoryMiddleware));
const store = createStore(reducer, applyMiddleware(thunkMiddleware, routerWithHistoryMiddleware));

export {store, history}
