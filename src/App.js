import React from 'react';
//import { List, Layout, Menu, Icon } from 'antd';
import { Route, Switch } from "react-router-dom";
import { Redirect } from 'react-router'
import { Provider } from 'react-redux'
//import axios from 'axios';
import { store, history } from './Store.js'
import './App.css';

import AppHeader from './AppHeader'
import AppFooter from './AppFooter'
import {view as LostsContent} from './lostsContent/'
import {view as Analysis} from './analysis/'
import {view as Department} from './department/'
import {view as About} from './about/'

import { ConnectedRouter } from 'react-router-redux'

import ReactGA from 'react-ga';

const NoMatch = ({ location }) => (
	<div>
		<h3>
			No match for <code>{location.pathname}</code>
		</h3>
	</div>
);

ReactGA.initialize('UA-17164901-8');

class App extends React.Component {
	/*
	constructor(props) {
		super(props)
	}
	*/

	render() {
		return (
			<Provider store={store}>
				<ConnectedRouter history={history}>
					<div>
						<AppHeader />
							<Switch>
								<Route exact path="/" render={() => (<Redirect to="/losts" />)} />
								<Route exact path='/losts' component={LostsContent} />
								<Route path='/losts/:id(\d+)' component={LostsContent} />
								<Route exact path='/analysis' render={() => (<Redirect to="/analysis/byCategory" />)} />
								<Route path='/analysis/by:by(Category|Department|Location|Date)' component={Analysis} />
								<Route exact path='/department' component={Department} />
								<Route exact path='/about' component={About} />
								<Route component={NoMatch} />
							</Switch>
						<AppFooter />
					</div>
				</ConnectedRouter>
			</Provider>
		);
	}
}

history.listen((location, action) => {
	ReactGA.pageview(location.pathname)
})

export default App;
