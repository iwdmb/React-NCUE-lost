import React from 'react';
import { Menu, Layout } from 'antd';
import { Link } from "react-router-dom";
import path from 'path'
//import axios from 'axios';
import { withRouter } from 'react-router'
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import {actions as lostsContentActions} from '../../analysis';
//import { enquireScreen } from '../../utils';

const SubMenu = Menu.SubMenu;
//const MenuItemGroup = Menu.ItemGroup;
const { Sider } = Layout;

class CustomSider extends React.Component {
	static propTypes = {
		match: PropTypes.object.isRequired,
		location: PropTypes.object.isRequired,
		history: PropTypes.object.isRequired
	}

	constructor(props) {
		super(...arguments);

		//const { match, location, history } = props;
		const { match } = props;

		this.state = {
			childArr: [],
			parentArr: [],
			locationPathDirName: path.dirname("/losts/*"),
			routerId: match.params.id
		};

		this.componentDidMount = this.componentDidMount.bind(this);
	}

	componentDidMount() {
		const defaultBy = (this.props.match.params.by).toLowerCase()
		this.props.onSelectSubCategory(defaultBy)
	}

	handleClick = (e) => {
		//const defaultBy = (this.props.match.params.by).toLowerCase()
		this.props.onSelectSubCategory(e.key.toLowerCase())
	}

	render() {
		//const {location, match} = this.props;
		const { match } = this.props;
		//const {childArr, parentArr, locationPathDirName} = this.state;

		return (
			<Sider breakpoint="lg" style={{background: '#ffffff'}} collapsedWidth="0" onCollapse={(collapsed, type) => { console.log(collapsed, type); }}>
				<Menu
					onClick={this.handleClick}
					style={{witdh: 256}}
					selectedKeys={[match.params.by]}
					defaultOpenKeys={['sub1']}
					mode="inline"
					theme="light"
				>
					<SubMenu key="sub1" title="統計">
						<Menu.Item key="Category">
							<Link to="/analysis/byCategory">
								物品分類
							</Link>
						</Menu.Item>
						<Menu.Item key="Department">
							<Link to="/analysis/byDepartment">
								單位
							</Link>
						</Menu.Item>
						<Menu.Item key="Location">
							<Link to="/analysis/byLocation">
								遺失地點
							</Link>
						</Menu.Item>
						<Menu.Item key="Date">
							<Link to="/analysis/byDate">
								日期
							</Link>
						</Menu.Item>
					</SubMenu>
				</Menu>
			</Sider>
		)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onSelectSubCategory: (by) => {
			dispatch(lostsContentActions.fetchLostsAnalysis(by))
		},
		clearLostsContentData: () => {
			dispatch(lostsContentActions.clearLostsAnalysisData())
		}
	}
}

export default withRouter(connect(null, mapDispatchToProps)(CustomSider))
