import {FETCH_STARTED, FETCH_SUCCESS, FETCH_FAILURE, CLEAR_LOSTSANALYSIS_DATA} from './actionTypes.js'
import axios from 'axios';

export const fetchLostsAnalysisStarted = () => ({
  type: FETCH_STARTED
});

export const fetchLostsAnalysisSuccess = (by, result, obj) => ({
	type: FETCH_SUCCESS,
	result,
	by,
	obj
})

export const fetchLostsAnalysisFailure = (error) => ({
  type: FETCH_FAILURE,
  error
})

export const clearLostsAnalysisData = () => ({
	type: CLEAR_LOSTSANALYSIS_DATA
})

export const fetchLostsAnalysis = (by) => {
	return (dispatch) => {
		const apiUrl = `${process.env.REACT_APP_API_URI}/v1/lostanalysis?by=${by}`

		dispatch(fetchLostsAnalysisStarted())

		axios({
			method: 'get',
			url: apiUrl,
			responseType: 'json'
		}).then((response) => {
			if (response.status !== 200) {
				throw new Error('Fail to get response with status ' + response.status);
			}

			let obj = {}
			obj[by] = response.data

			dispatch(fetchLostsAnalysisSuccess(by, response.data, obj))
		})

		/*.catch((error) => {
			dispatch(fetchLostsAnalysisFailure(error))
		})*/
	}
}
