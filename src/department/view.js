import React from 'react'
import { Table, Layout } from 'antd';
import axios from 'axios';
import './style.css';

class department extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			data: [],
		};

		this.componentDidMount = this.componentDidMount.bind(this);
	}

	componentDidMount() {
		axios({
			method: 'get',
			url: `${process.env.REACT_APP_API_URI}/v1/lostcontacts`,
			responseType: 'json'
		}).then(function(response) {
			let data = []

			response.data.forEach(function(element, index) {
				element['key'] = index

				data.push(element)
			})

			this.setState({...this.state, data: data})
		}.bind(this))

	}

	render() {
		const {data} = this.state;

		const columns = [{
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		}, {
			title: 'Department',
			dataIndex: 'department',
			key: 'department',
		}, {
			title: 'Location',
			dataIndex: 'location',
			key: 'location',
		}, {
			title: 'Tel',
			dataIndex: 'tel',
			key: 'tel',
		}];

		return (
			<Layout className="content">
				<h1>保管單位及承辦人員</h1>
				<Table columns={columns} dataSource={data} />
			</Layout>
		)
	}
}

export default department
