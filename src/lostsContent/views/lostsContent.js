import React from 'react'
import { Layout, Alert, Input } from 'antd';
import Content from './content.js'
import Sider from './sider.js'
import './style.css';

const Search = Input.Search


class lostsContent extends React.Component {
	/*
	constructor(props) {
		super(props)
	}
	*/

	render() {
		return (
			<Layout>
				<Sider />

				<Layout className="content">
				<div style={{ padding: 0, background: '#fff', margin: '0 0 0 0' }}>
					<Search placeholder="搜尋" onSearch={value => console.log(value)} enterButton disabled={true} />
				</div>
					<Alert style={{ margin: '24px 0 0 0' }} message="小提醒：同學領取失物時，請攜帶有照片之證件以茲證明，謝謝。" type="success" />
					<Content />
				</Layout>
			</Layout>
		)
	}
}

export default lostsContent
