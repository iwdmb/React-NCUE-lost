import React from 'react';
import { withRouter } from 'react-router'
import { connect } from 'react-redux';
import { Spin, Layout } from 'antd';
//import * as moment from 'moment';
import 'moment/locale/zh-tw'
//import path from 'path'
import * as Status from '../status.js'
import { actions as lostsAnalysisActions } from '../../analysis';
import { Chart, Axis, Geom, Tooltip, Coord, Legend, track } from 'bizcharts'
import DataSet from '@antv/data-set';

const { Content } = Layout;

track(false)

class CustomContent extends React.Component {
	constructor(props) {
		super(...arguments);

		this.state = {
			data: {}
		};
	}

	render() {
		//console.log(this.props.lostsAnalysisData)
		const { match, lostsAnalysisData } = this.props;
		const kvtable = {"lafing": "招領中", "gohome": "已認領", "expired": "已過期"}

		const ds = new DataSet()
		let cdldv = false
		let ddv = false

		if ((["Category", "Department", "Location"]).includes(match.params.by)) {
			if ((match.params.by).toLowerCase() in lostsAnalysisData.analysisContent) {
				if (!(match.params.by in this.state.data)) {
					this.state.data[match.params.by] = []
					let tempData = {}

					for (const [key, value] of Object.entries(lostsAnalysisData.analysisContent[match.params.by.toLowerCase()])) {

						value.forEach(function(element) {
							if (!(element["name"] in tempData)) {
								tempData[element["name"]] = {}
								tempData[element["name"]][kvtable["lafing"]] = 0
								tempData[element["name"]][kvtable["gohome"]] = 0
								tempData[element["name"]][kvtable["expired"]] = 0
							}

							tempData[element["name"]][kvtable[key]] += element["count"]
						});
					}

					for (const [key, value] of Object.entries(tempData)) {
						value["Item"] = key
						this.state.data[match.params.by].push(value)
					}

					//this.state.data[match.params.by].sort(function(a, b) {
					this.state.data[match.params.by].sort(function(a, b) {
						 return (a[kvtable["lafing"]] + a[kvtable["gohome"]] + a[kvtable["expired"]]) - (b[kvtable["lafing"]] + b[kvtable["gohome"]] + b[kvtable["expired"]])
					})
				}

				if (!(this.state.data[match.params.by] === undefined)) {
					cdldv = ds.createView().source(this.state.data[match.params.by]);
					cdldv.transform({
						type: 'fold',
						fields: [kvtable["lafing"], kvtable["gohome"], kvtable["expired"]],
						key: 'ItemStatus',
						value: 'ItemCount',
						retains: ['Item']
					})
				}
			}
		}

		//{name: "2018/04", count: 2}
		if ((["Date"]).includes(match.params.by)) {
			if ((match.params.by).toLowerCase() in lostsAnalysisData.analysisContent) {
				if (!(match.params.by in this.state.data)) {
					this.state.data[match.params.by] = []

					for (const [key, value] of Object.entries(lostsAnalysisData.analysisContent[match.params.by.toLowerCase()])) {
						value.forEach(function(element) {
							this.state.data[match.params.by].push({'date': element.name, 'count': element.count})
						}.bind(this))
					}
				}

				if ((match.params.by in this.state.data)) {
					ddv = this.state.data[match.params.by]
					//console.log(ddv)
				}
			}
		}

		return (
			<Content style={{ margin: '0px 0px 0px 0px' }}>
				<div style={{ padding: '0 0 0 0', background: '#fff', minHeight: 360 }}>
					<h1>相關分析</h1>
					{this.props.lostsAnalysisData.status === Status.LOADING &&
						<div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
							<Spin />
						</div>
					}
					{cdldv !== false &&
						<Chart height={400} data={cdldv} forceFit>
							<Legend />
							<Coord transpose />
							<Axis name="Item" label={{offset: 12}} />
							<Axis name="ItemCount" />
							<Tooltip />
							<Geom type="intervalStack" position="Item*ItemCount" color={'ItemStatus'} />
						</Chart>
					}

					{ddv !== false &&
						<Chart height={400} data={ddv} forceFit>
							<Axis name="date" />
							<Axis name="count" />
							<Tooltip crosshairs={{type : "y"}}/>
							<Geom type="line" position="date*count" size={2} />
							<Geom type='point' position="date*count" size={4} shape={'circle'} style={{ stroke: '#fff', lineWidth: 1}} />
						</Chart>
					}
				</div>
			</Content>
		)
	}
}

const mapStateToProps = (state) => {
	//console.log(state)
	//console.log("mstp")
	//console.log(state)
	return {
		lostsAnalysisData: state.analysis
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onLoadMore: (by) => {
			dispatch(lostsAnalysisActions.fetchLostsAnalysis(by))
		}
	}
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CustomContent))
