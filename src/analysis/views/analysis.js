import React from 'react'
import { Layout } from 'antd';
import Content from './content.js'
import Sider from './sider.js'
import './style.css';

//const Search = Input.Search

class lostsContent extends React.Component {
	/*
	constructor(props) {
		super(props)
	}
	*/

	render() {
		return (
			<Layout>
				<Sider />

				<Layout className="content">
					<Content />
				</Layout>
			</Layout>
		)
	}
}

export default lostsContent
